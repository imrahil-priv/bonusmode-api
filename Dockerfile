FROM microsoft/dotnet:2.2-sdk AS build
WORKDIR /app

### Building
COPY src/ src/
RUN dotnet restore src/BonusMode.WebApi

### Publish
RUN dotnet publish src/BonusMode.WebApi -c Release -o /app/out


### Running
FROM microsoft/dotnet:2.2-aspnetcore-runtime AS runtime
WORKDIR /app
COPY --from=build /app/out ./
ENTRYPOINT ["dotnet", "BonusMode.WebApi.dll"]

# BonusMode ASP.NET Core 2.2 API
Simple project loosely based on NorthwindTraders by Jason Taylor (https://github.com/JasonGT/NorthwindTraders)

### Build:
`docker build -t bonusmode_api .`

### Run:
`docker run -d -p 5000:5000 --name bonusmode_api bonusmode_api`

### MySQL + phpMyAdmin:
`docker-compose down && docker-compose up -d`

### Azure application environment variables:
`ASPNETCORE_ENVIRONMENT` -> `Production`  
`WEBSITES_PORT` -> `5000`   
`ConnectionStrings__BonusModeDatabase` -> 
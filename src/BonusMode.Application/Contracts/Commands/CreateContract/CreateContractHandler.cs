using System;
using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Contracts.Models;
using BonusMode.Application.Exceptions;
using BonusMode.Application.Interfaces;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BonusMode.Application.Contracts.Commands.CreateContract
{
    public class CreateContractHandler : IRequestHandler<CreateContractCommand>
    {
        private readonly BonusModeDbContext _context;
        private readonly IEntityExistService _entityExistService;

        public CreateContractHandler(
            BonusModeDbContext context,
            IEntityExistService entityExistService)
        {
            _context = context;
            _entityExistService = entityExistService;
        }

        public async Task<Unit> Handle(CreateContractCommand request, CancellationToken cancellationToken)
        {
            if (request.Contract.ContractType == null || request.Contract.ContractType.ContractTypeId == 0)
            {
                throw new ArgumentException("Missing contract type", nameof(ContractType));
            }

            if (!_entityExistService.ContractTypeExists(request.Contract.ContractType.ContractTypeId))
            {
                throw new NotFoundException(nameof(ContractType), request.Contract.ContractType.ContractTypeId);
            }

            Contract contract = new Contract 
            {
                Number = request.Contract.Number,
                Description = request.Contract.Description,
                Amount = request.Contract.Amount,
                ContractTypeId = request.Contract.ContractType.ContractTypeId,
                Guid = request.Guid
            };

            foreach (ContractEmployeeDto ce in request.Contract.ContractEmployees)
            {
                if (ce.Employee == null || ce.Employee.EmployeeId == 0 || !_entityExistService.EmployeeExists(ce.Employee.EmployeeId))
                {
                    throw new NotFoundException(nameof(Employee), ce.Employee.EmployeeId);
                }

                ContractEmployee contractEmployee = new ContractEmployee 
                {
                    EmployeeId = ce.Employee.EmployeeId,
                    ScopeValuation = ce.ScopeValuation,
                    ScopeMeasurement = ce.ScopeMeasurement,

                    Contract = contract
                };

                contract.ContractEmployees.Add(contractEmployee);
            }

            _context.Contracts.Add(contract);
            await _context.SaveChangesAsync();
   
            return Unit.Value;
        }
    }
}
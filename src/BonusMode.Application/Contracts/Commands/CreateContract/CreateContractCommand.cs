using System;
using BonusMode.Application.Contracts.Models;
using MediatR;

namespace BonusMode.Application.Contracts.Commands.CreateContract
{
    public class CreateContractCommand : IRequest
    {
        public Guid Guid { get; set; }
        public ContractDto Contract { get; set; }
    }
}

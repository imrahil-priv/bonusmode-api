using FluentValidation;
using System.Linq;

namespace BonusMode.Application.Contracts.Commands.UpdateContract
{
    public class UpdateContractCommandValidator : AbstractValidator<UpdateContractCommand>
    {
        public UpdateContractCommandValidator() 
        {
            RuleFor(c => c.Contract.Number).MaximumLength(6);
            RuleFor(c => c.Contract.ContractType).NotNull();
            RuleFor(c => c.Contract.ContractType.ContractTypeId).NotEmpty().When(c => c.Contract.ContractType != null);
            RuleFor(c => c.Contract.ContractEmployees).NotEmpty();

            RuleFor(x => x.Contract.ContractEmployees)
                .Must(coll => coll.Sum(item => item.ScopeValuation) == 100)
                .When(x => x.Contract.ContractEmployees != null)
                .WithMessage("Sum of valuations scopes for all employees should be 100.");

            RuleFor(x => x.Contract.ContractEmployees)
                .Must(coll => coll.Sum(item => item.ScopeMeasurement) == 100)
                .When(x => x.Contract.ContractEmployees != null)
                .WithMessage("Sum of measurement scopes for all employees should be 100.");
        }
    }
}

using BonusMode.Application.Contracts.Models;
using MediatR;

namespace BonusMode.Application.Contracts.Commands.UpdateContract
{
    public class UpdateContractCommand : IRequest
    {
        public int Id { get; set; }
        public ContractDto Contract { get; set; }
    }
}

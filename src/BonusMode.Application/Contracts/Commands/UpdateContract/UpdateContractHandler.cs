using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Contracts.Models;
using BonusMode.Application.Exceptions;
using BonusMode.Application.Interfaces;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace BonusMode.Application.Contracts.Commands.UpdateContract
{
    public class UpdateContractHandler : IRequestHandler<UpdateContractCommand>
    {
        private readonly BonusModeDbContext _context;
        private readonly IEntityExistService _entityExistService;
        private readonly ILogger<UpdateContractHandler> _logger;

        public UpdateContractHandler(
            BonusModeDbContext context,
            IEntityExistService entityExistService,
            ILogger<UpdateContractHandler> logger)
        {
            _context = context;
            _entityExistService = entityExistService;
            _logger = logger;
        }

        public async Task<Unit> Handle(UpdateContractCommand request, CancellationToken cancellationToken)
        {        
            if (request.Id != request.Contract.ContractId)
            {
                throw new ArgumentException("Mismatch between route id and contract id!");
            }

            if (request.Contract.ContractType == null || request.Contract.ContractType.ContractTypeId == 0 || !_entityExistService.ContractTypeExists(request.Contract.ContractType.ContractTypeId)) 
            {
                throw new NotFoundException(nameof(ContractType), request.Contract.ContractType.ContractTypeId);
            }

            Contract contract = await _context.Contracts
                .Include(c => c.ContractType)
                .Include(c => c.ContractEmployees)
                    .ThenInclude(ce => ce.Employee)
                .FirstOrDefaultAsync(c => c.ContractId == request.Id);

            if (contract != null)
            {
                // Update parent
                _context.Entry(contract).State = EntityState.Modified;

                // contract.Number = contractDto.Number;
                contract.Description = request.Contract.Description;
                contract.Amount = request.Contract.Amount;
                contract.Finished = request.Contract.Finished;
                contract.Accounted = request.Contract.Accounted;
                contract.ContractTypeId = request.Contract.ContractType.ContractTypeId;

                // Delete children
                foreach (ContractEmployee ce in contract.ContractEmployees.ToList())
                {
                    if (!request.Contract.ContractEmployees.Any(c => c.ContractEmployeeId == ce.ContractEmployeeId))
                    {
                        _logger.LogInformation("----------------------\nDelete child: {ID}\n----------------------", ce.ContractEmployeeId);
                        _context.ContractEmployees.Remove(ce);
                    }
                }

                // Update and Insert children
                foreach (ContractEmployeeDto ce in request.Contract.ContractEmployees)
                {
                    if (ce.Employee == null || ce.Employee.EmployeeId == 0 || !_entityExistService.EmployeeExists(ce.Employee.EmployeeId))
                    {
                        throw new NotFoundException(nameof(Employee), ce.Employee.EmployeeId);
                    }
                    
                    ContractEmployee existingContractEmployee = contract.ContractEmployees
                        .Where(c => c.ContractEmployeeId == ce.ContractEmployeeId)
                        .SingleOrDefault();

                    if (existingContractEmployee != null)
                    {
                        _logger.LogInformation("----------------------\nUpdate child: {ID}\n----------------------", ce.ContractEmployeeId);

                        // Update child
                        _context.Entry(existingContractEmployee).CurrentValues.SetValues(ce);

                        if (existingContractEmployee.EmployeeId != ce.Employee.EmployeeId)
                        {
                            existingContractEmployee.EmployeeId = ce.Employee.EmployeeId;
                        }
                    }
                    else
                    {
                        // Insert child
                        ContractEmployee newContractEmployee = new ContractEmployee
                        {
                            ContractId = contract.ContractId,
                            EmployeeId = ce.Employee.EmployeeId,
                            ScopeValuation = ce.ScopeValuation,
                            ScopeMeasurement = ce.ScopeMeasurement
                        };

                        contract.ContractEmployees.Add(newContractEmployee);
                    }
                }

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!_entityExistService.ContractExists(request.Id))
                    {
                        throw new NotFoundException(nameof(Contract), request.Id);
                    }
                    else
                    {
                        throw;
                    }
                }
            }
            else 
            {
                throw new NotFoundException(nameof(Contract), request.Id);
            }

            return Unit.Value;
        }
    }
}

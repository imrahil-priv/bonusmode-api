using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Exceptions;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using MediatR;

namespace BonusMode.Application.Contracts.Commands.DeleteContract
{
    public class DeleteContractHandler : IRequestHandler<DeleteContractCommand>
    {
        private readonly BonusModeDbContext _context;

        public DeleteContractHandler(BonusModeDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteContractCommand request, CancellationToken cancellationToken)
        {        
            Contract contract = await _context.Contracts.FindAsync(request.Id);
            
            if (contract == null)
            {
                throw new NotFoundException(nameof(Contract), request.Id);
            }

            _context.Contracts.Remove(contract);
            await _context.SaveChangesAsync();

            return Unit.Value;
        }
    }
}

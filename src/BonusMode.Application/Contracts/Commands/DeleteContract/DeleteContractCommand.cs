using MediatR;

namespace BonusMode.Application.Contracts.Commands.DeleteContract
{
    public class DeleteContractCommand : IRequest
    {
        public int Id { get; set; }
    }
}

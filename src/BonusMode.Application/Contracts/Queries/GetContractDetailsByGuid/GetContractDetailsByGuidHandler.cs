using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Contracts.Models;
using BonusMode.Application.Exceptions;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace BonusMode.Application.Contracts.Queries.GetContractDetailsByGuid
{
    public class GetContractDetailsByGuidHandler : IRequestHandler<GetContractDetailsByGuidQuery, ContractDto>
    {
        private readonly BonusModeDbContext _context;
        private readonly ILogger<GetContractDetailsByGuidHandler> _logger;

        public GetContractDetailsByGuidHandler(BonusModeDbContext context, ILogger<GetContractDetailsByGuidHandler> logger)
        {
            _context = context;
            _logger = logger;
        }
        
        public async Task<ContractDto> Handle(GetContractDetailsByGuidQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Getting contract by guid: {ID}", request.Guid);

            Contract contract = await _context.Contracts
                .Include(c => c.ContractType)
                .Include(c => c.ContractEmployees)
                    .ThenInclude(ce => ce.Employee)
                .FirstOrDefaultAsync(c => c.Guid == request.Guid);

            if (contract == null)
            {
                throw new NotFoundException(nameof(Contract), request.Guid);
            }

            return contract.Adapt<ContractDto>();
        }
    }
}

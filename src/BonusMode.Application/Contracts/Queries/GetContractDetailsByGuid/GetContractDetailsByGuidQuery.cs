using System;
using BonusMode.Application.Contracts.Models;
using MediatR;

namespace BonusMode.Application.Contracts.Queries.GetContractDetailsByGuid
{
    public class GetContractDetailsByGuidQuery : IRequest<ContractDto>
    {
        public Guid Guid { get; set; }
    }
}

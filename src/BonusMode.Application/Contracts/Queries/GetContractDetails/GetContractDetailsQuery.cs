using BonusMode.Application.Contracts.Models;
using MediatR;

namespace BonusMode.Application.Contracts.Queries.GetContractDetails
{
    public class GetContractDetailsQuery : IRequest<ContractDto>
    {
        public int Id { get; set; }
    }
}

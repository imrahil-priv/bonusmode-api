using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Contracts.Models;
using BonusMode.Application.Exceptions;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace BonusMode.Application.Contracts.Queries.GetContractDetails
{
    public class GetContractDetailsHandler : IRequestHandler<GetContractDetailsQuery, ContractDto>
    {
        private readonly BonusModeDbContext _context;
        private readonly ILogger<GetContractDetailsHandler> _logger;

        public GetContractDetailsHandler(BonusModeDbContext context, ILogger<GetContractDetailsHandler> logger)
        {
            _context = context;
            _logger = logger;
        }
        
        public async Task<ContractDto> Handle(GetContractDetailsQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Getting contract by id: {ID}", request.Id);

            Contract contract = await _context.Contracts
                .Include(c => c.ContractType)
                .Include(c => c.ContractEmployees)
                    .ThenInclude(ce => ce.Employee)
                .FirstOrDefaultAsync(c => c.ContractId == request.Id);

            if (contract == null)
            {
                throw new NotFoundException(nameof(Contract), request.Id);
            }

            return contract.Adapt<ContractDto>();
        }
    }
}

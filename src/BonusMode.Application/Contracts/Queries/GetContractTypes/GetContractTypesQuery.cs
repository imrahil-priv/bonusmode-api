using BonusMode.Application.Contracts.Models;
using MediatR;

namespace BonusMode.Application.Contracts.Queries.GetContractTypes
{
    public class GetContractTypesQuery : IRequest<ContractTypeListViewModel>
    {
    }
}

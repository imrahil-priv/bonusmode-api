using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Contracts.Models;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BonusMode.Application.Contracts.Queries.GetContractTypes
{
    public class GetContractTypesHandler : IRequestHandler<GetContractTypesQuery, ContractTypeListViewModel>
    {
        private readonly BonusModeDbContext _context;

        public GetContractTypesHandler(BonusModeDbContext context)
        {
            _context = context;
        }

        public async Task<ContractTypeListViewModel> Handle(GetContractTypesQuery request, CancellationToken cancellationToken)
        {
            List<ContractType> contractTypes = await _context.ContractTypes
                .ToListAsync();
            
            ContractTypeListViewModel model = new ContractTypeListViewModel
            {
                ContractTypes = contractTypes.Adapt<List<ContractTypeDto>>()
            };

            return model;
        }
    }
}

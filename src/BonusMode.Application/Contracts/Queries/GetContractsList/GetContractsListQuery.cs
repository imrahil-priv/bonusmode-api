using BonusMode.Application.Contracts.Models;
using MediatR;

namespace BonusMode.Application.Contracts.Queries.GetContractsList
{
    public class GetContractsListQuery : IRequest<ContractListViewModel>
    {
    }
}

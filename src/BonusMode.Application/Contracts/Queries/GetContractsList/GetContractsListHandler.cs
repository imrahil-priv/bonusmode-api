using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Contracts.Models;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BonusMode.Application.Contracts.Queries.GetContractsList
{
    public class GetContractsListHandler : IRequestHandler<GetContractsListQuery, ContractListViewModel>
    {
        private readonly BonusModeDbContext _context;

        public GetContractsListHandler(BonusModeDbContext context)
        {
            _context = context;
        }

        public async Task<ContractListViewModel> Handle(GetContractsListQuery request, CancellationToken cancellationToken)
        {
            List<Contract> contracts = await _context.Contracts
                .Include(c => c.ContractType)
                .Include(c => c.ContractEmployees)
                    .ThenInclude(ce => ce.Employee)
                .OrderBy(c => c.ContractId)
                .ToListAsync();
            
            ContractListViewModel model = new ContractListViewModel
            {
                Contracts = contracts.Adapt<List<ContractDto>>()
            };

            return model;
        }
    }
}

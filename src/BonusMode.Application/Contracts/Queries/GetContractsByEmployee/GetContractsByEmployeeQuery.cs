using BonusMode.Application.Contracts.Models;
using MediatR;

namespace BonusMode.Application.Contracts.Queries.GetContractsByEmployee
{
    public class GetContractsByEmployeeQuery : IRequest<ContractListViewModel>
    {
        public int Id { get; set; }
    }
}

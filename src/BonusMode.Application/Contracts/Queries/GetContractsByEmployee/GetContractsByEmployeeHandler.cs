using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Contracts.Models;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace BonusMode.Application.Contracts.Queries.GetContractsByEmployee
{
    public class GetContractsByEmployeeHandler : IRequestHandler<GetContractsByEmployeeQuery, ContractListViewModel>
    {
        private readonly BonusModeDbContext _context;
        private readonly ILogger<GetContractsByEmployeeHandler> _logger;

        public GetContractsByEmployeeHandler(BonusModeDbContext context, ILogger<GetContractsByEmployeeHandler> logger)
        {
            _context = context;
            _logger = logger;
        }
        
        public async Task<ContractListViewModel> Handle(GetContractsByEmployeeQuery request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("Getting contracts by employee id: {ID}", request.Id);

            List<Contract> contracts = await _context.Contracts
                .Include(c => c.ContractType)
                .Include(c => c.ContractEmployees)
                    .ThenInclude(ce => ce.Employee)
                .Where(c => c.ContractEmployees.Any(ce => ce.EmployeeId == request.Id))
                .ToListAsync();
            
            ContractListViewModel model = new ContractListViewModel
            {
                Contracts = contracts.Adapt<List<ContractDto>>()
            };

            return model;
        }
    }
}

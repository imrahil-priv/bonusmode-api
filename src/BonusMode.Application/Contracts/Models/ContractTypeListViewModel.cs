using System.Collections.Generic;

namespace BonusMode.Application.Contracts.Models
{
    public class ContractTypeListViewModel
    {
        public IEnumerable<ContractTypeDto> ContractTypes { get; set; }
    }
}

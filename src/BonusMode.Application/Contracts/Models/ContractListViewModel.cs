using System.Collections.Generic;

namespace BonusMode.Application.Contracts.Models
{
    public class ContractListViewModel
    {
        public IEnumerable<ContractDto> Contracts { get; set; }
    }
}

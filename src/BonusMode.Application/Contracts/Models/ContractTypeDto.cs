﻿using System;

namespace BonusMode.Application.Contracts.Models
{
    public partial class ContractTypeDto
    {
        public int ContractTypeId { get; set; }
        public string Name { get; set; }
    }
}

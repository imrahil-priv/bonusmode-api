﻿using System;
using BonusMode.Application.Employees.Models;

namespace BonusMode.Application.Contracts.Models
{
    public partial class ContractEmployeeDto
    {
        public int ContractEmployeeId { get; set; }
        public int ScopeValuation { get; set; }
        public int ScopeMeasurement { get; set; }

        public EmployeeDto Employee { get; set; }
    }
}

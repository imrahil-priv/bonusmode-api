using System;
using System.Collections.Generic;

namespace BonusMode.Application.Contracts.Models
{
    public class ContractDto
    {
        public int ContractId { get; set; }
        public string Number { get; set; }
        public string Description { get; set; }
        public float Amount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool Finished { get; set; }
        public bool Accounted { get; set; }

        public ContractTypeDto ContractType { get; set; }
        public ICollection<ContractEmployeeDto> ContractEmployees { get; set; }
    }
}

using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Employees.Models;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BonusMode.Application.Employees.Queries.GetEmployeesList
{
    public class GetEmployeesListHandler : IRequestHandler<GetEmployeesListQuery, EmployeeListViewModel>
    {
        private readonly BonusModeDbContext _context;

        public GetEmployeesListHandler(BonusModeDbContext context)
        {
            _context = context;
        }

        public async Task<EmployeeListViewModel> Handle(GetEmployeesListQuery request, CancellationToken cancellationToken)
        {
            List<Employee> employees = await _context.Employees
                .Include(e => e.Role)
                .OrderBy(e => e.EmployeeId)
                .ToListAsync();
     
            EmployeeListViewModel model = new EmployeeListViewModel
            {
                Employees = employees.Adapt<List<EmployeeDto>>()
            };

            return model;
        }        
    }
}

using BonusMode.Application.Employees.Models;
using MediatR;

namespace BonusMode.Application.Employees.Queries.GetEmployeesList
{
    public class GetEmployeesListQuery : IRequest<EmployeeListViewModel>
    {
    }
}

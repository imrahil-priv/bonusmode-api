using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Employees.Models;
using BonusMode.Application.Exceptions;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BonusMode.Application.Employees.Queries.GetEmployeeDetails
{
    public class GetEmployeeDetailsHandler : IRequestHandler<GetEmployeeDetailsQuery, EmployeeDto>
    {
        private readonly BonusModeDbContext _context;

        public GetEmployeeDetailsHandler(BonusModeDbContext context)
        {
            _context = context;
        }

        public async Task<EmployeeDto> Handle(GetEmployeeDetailsQuery request, CancellationToken cancellationToken)
        {
            Employee employee = await _context.Employees
                .Include(e => e.Role)
                .FirstOrDefaultAsync(e => e.EmployeeId == request.Id);

            if (employee == null)
            {
                throw new NotFoundException(nameof(Employee), request.Id);
            }

            return employee.Adapt<EmployeeDto>();
        }
    }
}

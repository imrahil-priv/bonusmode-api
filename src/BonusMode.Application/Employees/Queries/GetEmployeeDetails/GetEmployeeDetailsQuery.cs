using BonusMode.Application.Employees.Models;
using MediatR;

namespace BonusMode.Application.Employees.Queries.GetEmployeeDetails
{
    public class GetEmployeeDetailsQuery : IRequest<EmployeeDto>
    {
        public int Id { get; set; }
    }
}

using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Exceptions;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using MediatR;

namespace BonusMode.Application.Employees.Commands.DeleteEmployee
{
    public class DeleteEmployeeHandler : IRequestHandler<DeleteEmployeeCommand>
    {
        private readonly BonusModeDbContext _context;

        public DeleteEmployeeHandler(BonusModeDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeleteEmployeeCommand request, CancellationToken cancellationToken)
        {        
            Employee employee = await _context.Employees.FindAsync(request.Id);
            
            if (employee == null)
            {
                throw new NotFoundException(nameof(Employee), request.Id);
            }

            var hasContracts = _context.ContractEmployees.Any(o => o.EmployeeId == employee.EmployeeId);
            if (hasContracts)
            {
                throw new DeleteFailureException(nameof(Employee), request.Id, "There are existing contracts associated with this employee.");
            }

            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();

            return Unit.Value;
        }
    }
}

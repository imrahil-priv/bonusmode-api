using System;
using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Exceptions;
using BonusMode.Application.Interfaces;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BonusMode.Application.Employees.Commands.CreateEmployee
{
    public class CreateEmployeeHandler : IRequestHandler<CreateEmployeeCommand>
    {
        private readonly BonusModeDbContext _context;
        private readonly IEntityExistService _entityExistService;

        public CreateEmployeeHandler(
            BonusModeDbContext context,
            IEntityExistService entityExistService)
        {
            _context = context;
            _entityExistService = entityExistService;
        }

        public async Task<Unit> Handle(CreateEmployeeCommand request, CancellationToken cancellationToken)
        {
            if (request.Employee.Role == null || request.Employee.Role.RoleId == 0)
            {
                throw new ArgumentException("Missing role", nameof(Role));
            }

            if (!_entityExistService.RoleExists(request.Employee.Role.RoleId))
            {
                throw new NotFoundException(nameof(Role), request.Employee.Role.RoleId);
            }

            Employee employee = new Employee
            {
                FirstName = request.Employee.FirstName,
                LastName = request.Employee.LastName,
                RoleId = request.Employee.Role.RoleId
            };

            _context.Employees.Add(employee);
            await _context.SaveChangesAsync();
   
            return Unit.Value;
        }
    }
}

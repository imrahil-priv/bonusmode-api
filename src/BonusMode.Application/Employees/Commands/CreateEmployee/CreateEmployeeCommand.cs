using BonusMode.Application.Employees.Models;
using MediatR;

namespace BonusMode.Application.Employees.Commands.CreateEmployee
{
    public class CreateEmployeeCommand : IRequest
    {
        public EmployeeDto Employee { get; set; }
    }
}

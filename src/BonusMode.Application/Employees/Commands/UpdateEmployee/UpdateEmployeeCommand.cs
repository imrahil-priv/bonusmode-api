using BonusMode.Application.Employees.Models;
using MediatR;

namespace BonusMode.Application.Employees.Commands.UpdateEmployee
{
    public class UpdateEmployeeCommand : IRequest
    {
        public int Id { get; set; }
        public EmployeeDto Employee { get; set; }        
    }
}

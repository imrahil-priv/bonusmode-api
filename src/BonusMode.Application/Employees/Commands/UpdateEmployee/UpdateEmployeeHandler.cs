using System;
using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Employees.Models;
using BonusMode.Application.Exceptions;
using BonusMode.Application.Interfaces;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BonusMode.Application.Employees.Commands.UpdateEmployee
{
    public class UpdateEmployeeHandler : IRequestHandler<UpdateEmployeeCommand>
    {
        private readonly BonusModeDbContext _context;
        private readonly IEntityExistService _entityExistService;

        public UpdateEmployeeHandler(
            BonusModeDbContext context,
            IEntityExistService entityExistService)
        {
            _context = context;
            _entityExistService = entityExistService;
        }

        public async Task<Unit> Handle(UpdateEmployeeCommand request, CancellationToken cancellationToken)
        {
            if (request.Id != request.Employee.EmployeeId)
            {
                throw new ArgumentException("Mismatch between route id and employee id!");
            }

            if ( request.Employee.Role == null || request.Employee.Role.RoleId == 0 || !_entityExistService.RoleExists(request.Employee.Role.RoleId))
            {
                throw new NotFoundException(nameof(Role), request.Employee.Role.RoleId);
            }

            Employee employee = await _context.Employees
                .Include(e => e.Role)
                .FirstOrDefaultAsync(e => e.EmployeeId == request.Id);

            if (employee != null)
            {
                // Update parent
                _context.Entry(employee).CurrentValues.SetValues(request.Employee);
                _context.Entry(employee).State = EntityState.Modified;

                if (request.Employee.Role.RoleId != employee.Role.RoleId)
                {
                    // find role
                    Role role = await _context.Roles
                        .FirstOrDefaultAsync(r => r.RoleId == request.Employee.Role.RoleId);
        
                    // update parent with new role
                    employee.Role = role;
                }

                try
                {
                    // save to db
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }
            else 
            {
                throw new NotFoundException(nameof(Employee), request.Id);
            }

            return Unit.Value;
        }
    }
}

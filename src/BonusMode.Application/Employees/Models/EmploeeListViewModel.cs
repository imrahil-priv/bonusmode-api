using System.Collections.Generic;

namespace BonusMode.Application.Employees.Models
{
    public class EmployeeListViewModel
    {
        public IEnumerable<EmployeeDto> Employees { get; set; }
    }
}

﻿using BonusMode.Application.Roles.Models;

namespace BonusMode.Application.Employees.Models
{
    public partial class EmployeeDto
    {
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public RoleDto Role { get; set; }
    }
}

using MediatR;
using BonusMode.Application.Roles.Models;

namespace BonusMode.Application.Roles.Queries.GetRolesList
{
    public class GetRolesListQuery : IRequest<RolesListViewModel>
    {
    }
}

using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Roles.Models;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BonusMode.Application.Roles.Queries.GetRolesList
{
    public class GetRolesListHandler : IRequestHandler<GetRolesListQuery, RolesListViewModel>
    {
        private readonly BonusModeDbContext _context;

        public GetRolesListHandler(BonusModeDbContext context)
        {
            _context = context;
        }

        public async Task<RolesListViewModel> Handle(GetRolesListQuery request, CancellationToken cancellationToken)
        {
            List<Role> roles = await _context.Roles
                .ToListAsync();
     
            RolesListViewModel model = new RolesListViewModel
            {
                Roles = roles.Adapt<List<RoleDto>>()
            };

            return model;
        }
    }
}

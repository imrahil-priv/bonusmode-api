﻿using System;
using System.Collections.Generic;

namespace BonusMode.Application.Roles.Models
{
    public partial class RoleDto
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
    }
}

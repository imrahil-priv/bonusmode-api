using System.Collections.Generic;

namespace BonusMode.Application.Roles.Models
{
    public class RolesListViewModel
    {
        public IEnumerable<RoleDto> Roles { get; set; }
    }
}

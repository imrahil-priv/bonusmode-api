using BonusMode.Application.Settings.Models;
using MediatR;

namespace BonusMode.Application.Settings.Queries.GetSettings
{
    public class GetSettingsQuery : IRequest<SettingDto>
    {
    }
}

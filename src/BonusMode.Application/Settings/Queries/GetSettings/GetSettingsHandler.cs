using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Exceptions;
using BonusMode.Application.Settings.Models;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using Mapster;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BonusMode.Application.Settings.Queries.GetSettings
{
    public class GetSettingsHandler : IRequestHandler<GetSettingsQuery, SettingDto>
    {
        private readonly BonusModeDbContext _context;

        public GetSettingsHandler(BonusModeDbContext context)
        {
            _context = context;
        }

        public async Task<SettingDto> Handle(GetSettingsQuery request, CancellationToken cancellationToken)
        {
            Setting setting = await _context.Settings.FirstAsync();

            if (setting == null)
            {
                throw new NotFoundException(nameof(Setting), "");
            }

            return setting.Adapt<SettingDto>();
        }
    }
}

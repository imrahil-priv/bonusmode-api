using BonusMode.Application.Settings.Models;
using MediatR;

namespace BonusMode.Application.Settings.Commands.UpdateSettings
{
    public class UpdateSettingsCommand : IRequest
    {
        public SettingDto Setting { get; set; }        
    }
}

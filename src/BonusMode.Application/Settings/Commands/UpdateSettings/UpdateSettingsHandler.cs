using System.Threading;
using System.Threading.Tasks;
using BonusMode.Application.Exceptions;
using BonusMode.Domain.Entities;
using BonusMode.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace BonusMode.Application.Settings.Commands.UpdateSettings
{
    public class UpdateSettingsHandler : IRequestHandler<UpdateSettingsCommand>
    {
        private readonly BonusModeDbContext _context;

        public UpdateSettingsHandler(BonusModeDbContext context)
        {
            _context = context;
        }
        
        public async Task<Unit> Handle(UpdateSettingsCommand request, CancellationToken cancellationToken)
        {
            Setting settings = await _context.Settings
                .FirstOrDefaultAsync(s => s.SettingId == request.Setting.SettingId);

            if (settings != null)
            {
                _context.Entry(settings).CurrentValues.SetValues(request.Setting);

                try
                {
                    // save to db
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }
            }
            else 
            {
                throw new NotFoundException(nameof(Setting), request.Setting.SettingId);
            }

            return Unit.Value;
        }
    }
}

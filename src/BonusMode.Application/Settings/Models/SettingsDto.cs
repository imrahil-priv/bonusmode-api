namespace BonusMode.Application.Settings.Models
{
    public class SettingDto
    {
        public int SettingId { get; set; }

        public float ValuationBonus { get; set; }
        public float MeasurementBonus { get; set; }
        public float FinalizationBonus { get; set; }
    }
}

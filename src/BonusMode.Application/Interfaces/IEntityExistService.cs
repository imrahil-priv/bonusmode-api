namespace BonusMode.Application.Interfaces
{
    public interface IEntityExistService
    {
        bool ContractExists(int id);
        bool ContractTypeExists(int id);
        bool EmployeeExists(int id);
        bool RoleExists(int id);
    }
}

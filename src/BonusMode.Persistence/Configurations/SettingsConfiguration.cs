using BonusMode.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace BonusMode.Persistence.Configurations
{
    public class SettingsConfiguration : IEntityTypeConfiguration<Setting>
    {
        public void Configure(EntityTypeBuilder<Setting> builder)
        {
            builder.HasKey(e => e.SettingId);

            builder.ToTable("Settings");
    
            builder.Property(e => e.SettingId).HasColumnType("int(11)");
        }
    }
}

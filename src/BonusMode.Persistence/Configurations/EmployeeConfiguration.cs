using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BonusMode.Domain.Entities;

namespace BonusMode.Persistence.Configurations
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> builder)
        {
            builder.HasKey(e => e.EmployeeId);

            builder.ToTable("Employees");

            builder.HasIndex(e => e.RoleId)
                .HasName("fk_Employees_1_idx");

            builder.Property(e => e.EmployeeId).HasColumnType("int(11)");

            builder.Property(e => e.FirstName)
                .IsRequired()
                .HasMaxLength(65)
                .IsUnicode(false);

            builder.Property(e => e.LastName)
                .IsRequired()
                .HasMaxLength(65)
                .IsUnicode(false);

            builder.Property(e => e.RoleId).HasColumnType("int(11)");

            builder.HasOne(d => d.Role)
                .WithMany(p => p.Employees)
                .HasForeignKey(d => d.RoleId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_Employees_1");
        }
    }
}

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BonusMode.Domain.Entities;

namespace BonusMode.Persistence.Configurations
{
    public class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.HasKey(e => e.RoleId);

            builder.ToTable("Roles");

            builder.Property(e => e.RoleId).HasColumnType("int(11)");

            builder.Property(e => e.RoleName)
                .IsRequired()
                .HasMaxLength(65)
                .IsUnicode(false);
        }
    }
}

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BonusMode.Domain.Entities;

namespace BonusMode.Persistence.Configurations
{
    public class ContractEmployeeConfiguration : IEntityTypeConfiguration<ContractEmployee>
    {
        public void Configure(EntityTypeBuilder<ContractEmployee> builder)
        {
            builder.ToTable("ContractEmployee");

            builder.HasIndex(e => e.ContractId)
                .HasName("fk_ContractEmployee_1_idx");

            builder.HasIndex(e => e.EmployeeId)
                .HasName("fk_ContractEmployee_2_idx");

            builder.Property(e => e.ContractEmployeeId).HasColumnType("int(11)");

            builder.Property(e => e.ContractId).HasColumnType("int(11)");

            builder.Property(e => e.EmployeeId).HasColumnType("int(11)");

            builder.Property(e => e.ScopeMeasurement)
                .HasColumnType("int(11)")
                .HasDefaultValueSql("0");

            builder.Property(e => e.ScopeValuation)
                .HasColumnType("int(11)")
                .HasDefaultValueSql("0");

            builder.HasOne(d => d.Contract)
                .WithMany(p => p.ContractEmployees)
                .HasForeignKey(d => d.ContractId)
                .OnDelete(DeleteBehavior.Cascade)
                .HasConstraintName("fk_ContractEmployee_ContractId");

            builder.HasOne(d => d.Employee)
                .WithMany(p => p.ContractEmployees)
                .HasForeignKey(d => d.EmployeeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_ContractEmployee_EmployeeId");
        }
    }
}

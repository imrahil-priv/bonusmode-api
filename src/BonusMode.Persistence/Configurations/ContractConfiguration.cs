using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using BonusMode.Domain.Entities;

namespace BonusMode.Persistence.Configurations
{
    public class ContractConfiguration : IEntityTypeConfiguration<Contract>
    {
        public void Configure(EntityTypeBuilder<Contract> builder)
        {
            builder.HasKey(e => e.ContractId);

            builder.ToTable("Contracts");

            builder.HasIndex(e => e.ContractTypeId)
                .HasName("fk_Contract_1_idx");

            builder.Property(e => e.ContractId).HasColumnType("int(11)");

            builder.Property(e => e.ContractTypeId).HasColumnType("int(11)");

            builder.Property(e => e.CreatedAt).HasDefaultValueSql("CURRENT_TIMESTAMP");

            builder.Property(e => e.Description).IsUnicode(false);

            builder.Property(e => e.Finished)
                .HasColumnType("bit(1)")
                .HasDefaultValueSql("b'0'");

            builder.Property(e => e.Accounted)
                .HasColumnType("bit(1)")
                .HasDefaultValueSql("b'0'");

            builder.Property(e => e.Number)
                .IsRequired()
                .HasMaxLength(45)
                .IsUnicode(false);

            builder.Property(e => e.UpdatedAt).HasDefaultValueSql("CURRENT_TIMESTAMP");

            builder.HasOne(d => d.ContractType)
                .WithMany(p => p.Contracts)
                .HasForeignKey(d => d.ContractTypeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_Contract_1");
        }
    }
}

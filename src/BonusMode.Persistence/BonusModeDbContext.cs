﻿using Microsoft.EntityFrameworkCore;
using BonusMode.Domain.Entities;

namespace BonusMode.Persistence
{
    public partial class BonusModeDbContext : DbContext
    {
        public BonusModeDbContext(DbContextOptions<BonusModeDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Contract> Contracts { get; set; }
        public virtual DbSet<ContractType> ContractTypes { get; set; }
        public virtual DbSet<ContractEmployee> ContractEmployees { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(BonusModeDbContext).Assembly);
        }
    }
}

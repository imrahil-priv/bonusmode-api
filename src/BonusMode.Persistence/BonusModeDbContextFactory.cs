﻿using Microsoft.EntityFrameworkCore;
using BonusMode.Domain.Entities;
using BonusMode.Persistence.Infrastructure;

namespace BonusMode.Persistence
{
    public class BonusModeDbContextFactory : DesignTimeDbContextFactoryBase<BonusModeDbContext>
    {
        protected override BonusModeDbContext CreateNewInstance(DbContextOptions<BonusModeDbContext> options)
        {
            return new BonusModeDbContext(options);
        }
    }
}

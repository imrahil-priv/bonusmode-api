﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BonusMode.Persistence.Migrations
{
    public partial class BetterConstraintAndNewColumn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_ContractEmployee_ContractId",
                schema: "bonusmode",
                table: "ContractEmployee");

            migrationBuilder.AddColumn<bool>(
                name: "Accounted",
                schema: "bonusmode",
                table: "Contracts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "fk_ContractEmployee_ContractId",
                schema: "bonusmode",
                table: "ContractEmployee",
                column: "ContractId",
                principalSchema: "bonusmode",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_ContractEmployee_ContractId",
                schema: "bonusmode",
                table: "ContractEmployee");

            migrationBuilder.DropColumn(
                name: "Accounted",
                schema: "bonusmode",
                table: "Contracts");

            migrationBuilder.AddForeignKey(
                name: "fk_ContractEmployee_ContractId",
                schema: "bonusmode",
                table: "ContractEmployee",
                column: "ContractId",
                principalSchema: "bonusmode",
                principalTable: "Contracts",
                principalColumn: "ContractId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BonusMode.Persistence.Migrations
{
    public partial class AddedGuidToContract : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameTable(
                name: "Roles",
                schema: "bonusmode",
                newName: "Roles");

            migrationBuilder.RenameTable(
                name: "Employees",
                schema: "bonusmode",
                newName: "Employees");

            migrationBuilder.RenameTable(
                name: "ContractTypes",
                schema: "bonusmode",
                newName: "ContractTypes");

            migrationBuilder.RenameTable(
                name: "Contracts",
                schema: "bonusmode",
                newName: "Contracts");

            migrationBuilder.RenameTable(
                name: "ContractEmployee",
                schema: "bonusmode",
                newName: "ContractEmployee");

            migrationBuilder.AlterColumn<bool>(
                name: "Accounted",
                table: "Contracts",
                type: "bit(1)",
                nullable: false,
                defaultValueSql: "b'0'",
                oldClrType: typeof(bool));

            migrationBuilder.AddColumn<Guid>(
                name: "Guid",
                table: "Contracts",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Guid",
                table: "Contracts");

            migrationBuilder.EnsureSchema(
                name: "bonusmode");

            migrationBuilder.RenameTable(
                name: "Roles",
                newName: "Roles",
                newSchema: "bonusmode");

            migrationBuilder.RenameTable(
                name: "Employees",
                newName: "Employees",
                newSchema: "bonusmode");

            migrationBuilder.RenameTable(
                name: "ContractTypes",
                newName: "ContractTypes",
                newSchema: "bonusmode");

            migrationBuilder.RenameTable(
                name: "Contracts",
                newName: "Contracts",
                newSchema: "bonusmode");

            migrationBuilder.RenameTable(
                name: "ContractEmployee",
                newName: "ContractEmployee",
                newSchema: "bonusmode");

            migrationBuilder.AlterColumn<bool>(
                name: "Accounted",
                schema: "bonusmode",
                table: "Contracts",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "bit(1)",
                oldDefaultValueSql: "b'0'");
        }
    }
}

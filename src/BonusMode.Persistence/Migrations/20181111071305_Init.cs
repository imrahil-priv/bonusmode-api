﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BonusMode.Persistence.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "bonusmode");

            migrationBuilder.CreateTable(
                name: "ContractTypes",
                schema: "bonusmode",
                columns: table => new
                {
                    ContractTypeId = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(unicode: false, maxLength: 45, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractTypes", x => x.ContractTypeId);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                schema: "bonusmode",
                columns: table => new
                {
                    RoleId = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RoleName = table.Column<string>(unicode: false, maxLength: 65, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.RoleId);
                });

            migrationBuilder.CreateTable(
                name: "Contracts",
                schema: "bonusmode",
                columns: table => new
                {
                    ContractId = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Number = table.Column<string>(unicode: false, maxLength: 45, nullable: false),
                    Description = table.Column<string>(unicode: false, nullable: true),
                    Amount = table.Column<float>(nullable: false),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP"),
                    UpdatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"),
                    Finished = table.Column<bool>(type: "bit(1)", nullable: false, defaultValueSql: "b'0'"),
                    ContractTypeId = table.Column<int>(type: "int(11)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Contracts", x => x.ContractId);
                    table.ForeignKey(
                        name: "fk_Contract_1",
                        column: x => x.ContractTypeId,
                        principalSchema: "bonusmode",
                        principalTable: "ContractTypes",
                        principalColumn: "ContractTypeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Employees",
                schema: "bonusmode",
                columns: table => new
                {
                    EmployeeId = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(unicode: false, maxLength: 65, nullable: false),
                    LastName = table.Column<string>(unicode: false, maxLength: 65, nullable: false),
                    RoleId = table.Column<int>(type: "int(11)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employees", x => x.EmployeeId);
                    table.ForeignKey(
                        name: "fk_Employees_1",
                        column: x => x.RoleId,
                        principalSchema: "bonusmode",
                        principalTable: "Roles",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ContractEmployee",
                schema: "bonusmode",
                columns: table => new
                {
                    ContractEmployeeId = table.Column<int>(type: "int(11)", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ContractId = table.Column<int>(type: "int(11)", nullable: false),
                    EmployeeId = table.Column<int>(type: "int(11)", nullable: false),
                    ScopeValuation = table.Column<int>(type: "int(11)", nullable: false, defaultValueSql: "0"),
                    ScopeMeasurement = table.Column<int>(type: "int(11)", nullable: false, defaultValueSql: "0")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ContractEmployee", x => x.ContractEmployeeId);
                    table.ForeignKey(
                        name: "fk_ContractEmployee_ContractId",
                        column: x => x.ContractId,
                        principalSchema: "bonusmode",
                        principalTable: "Contracts",
                        principalColumn: "ContractId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_ContractEmployee_EmployeeId",
                        column: x => x.EmployeeId,
                        principalSchema: "bonusmode",
                        principalTable: "Employees",
                        principalColumn: "EmployeeId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "fk_ContractEmployee_1_idx",
                schema: "bonusmode",
                table: "ContractEmployee",
                column: "ContractId");

            migrationBuilder.CreateIndex(
                name: "fk_ContractEmployee_2_idx",
                schema: "bonusmode",
                table: "ContractEmployee",
                column: "EmployeeId");

            migrationBuilder.CreateIndex(
                name: "fk_Contract_1_idx",
                schema: "bonusmode",
                table: "Contracts",
                column: "ContractTypeId");

            migrationBuilder.CreateIndex(
                name: "fk_Employees_1_idx",
                schema: "bonusmode",
                table: "Employees",
                column: "RoleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ContractEmployee",
                schema: "bonusmode");

            migrationBuilder.DropTable(
                name: "Contracts",
                schema: "bonusmode");

            migrationBuilder.DropTable(
                name: "Employees",
                schema: "bonusmode");

            migrationBuilder.DropTable(
                name: "ContractTypes",
                schema: "bonusmode");

            migrationBuilder.DropTable(
                name: "Roles",
                schema: "bonusmode");
        }
    }
}

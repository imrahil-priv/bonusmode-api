﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using BonusMode.Domain;

namespace BonusMode.Persistence.Migrations
{
    [DbContext(typeof(BonusModeDbContext))]
    [Migration("20181111213331_BetterConstraintAndNewColumn")]
    partial class BetterConstraintAndNewColumn
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.1.4-rtm-31024")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("BonusMode.Models.Contract", b =>
                {
                    b.Property<int>("ContractId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int(11)");

                    b.Property<bool>("Accounted");

                    b.Property<float>("Amount");

                    b.Property<int>("ContractTypeId")
                        .HasColumnType("int(11)");

                    b.Property<DateTime>("CreatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("CURRENT_TIMESTAMP");

                    b.Property<string>("Description")
                        .IsUnicode(false);

                    b.Property<bool>("Finished")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bit(1)")
                        .HasDefaultValueSql("b'0'");

                    b.Property<string>("Number")
                        .IsRequired()
                        .HasMaxLength(45)
                        .IsUnicode(false);

                    b.Property<DateTime>("UpdatedAt")
                        .ValueGeneratedOnAdd()
                        .HasDefaultValueSql("CURRENT_TIMESTAMP");

                    b.HasKey("ContractId");

                    b.HasIndex("ContractTypeId")
                        .HasName("fk_Contract_1_idx");

                    b.ToTable("Contracts","bonusmode");
                });

            modelBuilder.Entity("BonusMode.Models.ContractEmployee", b =>
                {
                    b.Property<int>("ContractEmployeeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int(11)");

                    b.Property<int>("ContractId")
                        .HasColumnType("int(11)");

                    b.Property<int>("EmployeeId")
                        .HasColumnType("int(11)");

                    b.Property<int>("ScopeMeasurement")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int(11)")
                        .HasDefaultValueSql("0");

                    b.Property<int>("ScopeValuation")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int(11)")
                        .HasDefaultValueSql("0");

                    b.HasKey("ContractEmployeeId");

                    b.HasIndex("ContractId")
                        .HasName("fk_ContractEmployee_1_idx");

                    b.HasIndex("EmployeeId")
                        .HasName("fk_ContractEmployee_2_idx");

                    b.ToTable("ContractEmployee","bonusmode");
                });

            modelBuilder.Entity("BonusMode.Models.ContractType", b =>
                {
                    b.Property<int>("ContractTypeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int(11)");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasMaxLength(45)
                        .IsUnicode(false);

                    b.HasKey("ContractTypeId");

                    b.ToTable("ContractTypes","bonusmode");
                });

            modelBuilder.Entity("BonusMode.Models.Employee", b =>
                {
                    b.Property<int>("EmployeeId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int(11)");

                    b.Property<string>("FirstName")
                        .IsRequired()
                        .HasMaxLength(65)
                        .IsUnicode(false);

                    b.Property<string>("LastName")
                        .IsRequired()
                        .HasMaxLength(65)
                        .IsUnicode(false);

                    b.Property<int>("RoleId")
                        .HasColumnType("int(11)");

                    b.HasKey("EmployeeId");

                    b.HasIndex("RoleId")
                        .HasName("fk_Employees_1_idx");

                    b.ToTable("Employees","bonusmode");
                });

            modelBuilder.Entity("BonusMode.Models.Role", b =>
                {
                    b.Property<int>("RoleId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int(11)");

                    b.Property<string>("RoleName")
                        .IsRequired()
                        .HasMaxLength(65)
                        .IsUnicode(false);

                    b.HasKey("RoleId");

                    b.ToTable("Roles","bonusmode");
                });

            modelBuilder.Entity("BonusMode.Models.Contract", b =>
                {
                    b.HasOne("BonusMode.Models.ContractType", "ContractType")
                        .WithMany("Contracts")
                        .HasForeignKey("ContractTypeId")
                        .HasConstraintName("fk_Contract_1");
                });

            modelBuilder.Entity("BonusMode.Models.ContractEmployee", b =>
                {
                    b.HasOne("BonusMode.Models.Contract", "Contract")
                        .WithMany("ContractEmployees")
                        .HasForeignKey("ContractId")
                        .HasConstraintName("fk_ContractEmployee_ContractId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("BonusMode.Models.Employee", "Employee")
                        .WithMany("ContractEmployees")
                        .HasForeignKey("EmployeeId")
                        .HasConstraintName("fk_ContractEmployee_EmployeeId");
                });

            modelBuilder.Entity("BonusMode.Models.Employee", b =>
                {
                    b.HasOne("BonusMode.Models.Role", "Role")
                        .WithMany("Employees")
                        .HasForeignKey("RoleId")
                        .HasConstraintName("fk_Employees_1");
                });
#pragma warning restore 612, 618
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BonusMode.Persistence.Migrations
{
    public partial class RenamedSettingEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SettingsId",
                table: "Settings",
                newName: "SettingId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "SettingId",
                table: "Settings",
                newName: "SettingsId");
        }
    }
}

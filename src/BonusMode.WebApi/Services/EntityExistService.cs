using System.Linq;
using BonusMode.Application.Interfaces;
using BonusMode.Persistence;

namespace BonusMode.WebApi.Services
{
    public class EntityExistService : IEntityExistService
    {
        private readonly BonusModeDbContext _context;

        public EntityExistService(BonusModeDbContext context)
        {
            _context = context;
        }

        public bool ContractExists(int id)
        {
            return _context.Contracts.Any(e => e.ContractId == id);
        }

        public bool ContractTypeExists(int id)
        {
            return _context.ContractTypes.Any(e => e.ContractTypeId == id);
        }

        public bool EmployeeExists(int id)
        {
            return _context.Employees.Any(e => e.EmployeeId == id);
        }

        public bool RoleExists(int id)
        {
            return _context.Roles.Any(e => e.RoleId == id);
        }
    }
}

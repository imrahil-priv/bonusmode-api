using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BonusMode.Persistence;
using BonusMode.Application.Employees.Models;
using BonusMode.Domain.Entities;
using BonusMode.Application.Contracts.Models;
using BonusMode.Application.Contracts.Commands.CreateContract;
using BonusMode.Application.Contracts.Commands.UpdateContract;
using BonusMode.Application.Contracts.Commands.DeleteContract;
using BonusMode.Application.Contracts.Queries.GetContractsList;
using BonusMode.Application.Contracts.Queries.GetContractsByEmployee;
using BonusMode.Application.Contracts.Queries.GetContractTypes;
using BonusMode.Application.Contracts.Queries.GetContractDetails;
using BonusMode.Application.Contracts.Queries.GetContractDetailsByGuid;

namespace BonusMode.WebApi.Controllers
{
    public class ContractsController : BaseController
    {
        // GET: api/contracts
        [HttpGet]
        public async Task<ActionResult<ContractListViewModel>> GetContracts()
        {
            return Ok(await Mediator.Send(new GetContractsListQuery()));
        }

        // GET: api/contracts/by/employee
        [HttpGet("by/employee/{id}")]
        public async Task<ActionResult<ContractListViewModel>> GetContractsByEmployee([FromRoute] int id)
        {
            return Ok(await Mediator.Send(new GetContractsByEmployeeQuery { Id = id }));
        }

        // GET: api/contracts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ContractDto>> GetContractDetails([FromRoute] int id)
        {
            return Ok(await Mediator.Send(new GetContractDetailsQuery { Id = id }));
        }

        // POST: api/contracts
        [HttpPost]
        public async Task<IActionResult> CreateContract([FromBody] ContractDto contractDto)
        {
            Guid guid = Guid.NewGuid();
            await Mediator.Send(new CreateContractCommand { Guid = guid, Contract = contractDto });

            return Ok(await Mediator.Send(new GetContractDetailsByGuidQuery { Guid = guid }));
        }

        // PUT: api/contracts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateContract([FromRoute] int id, [FromBody] ContractDto contractDto)
        {
            await Mediator.Send(new UpdateContractCommand { 
                Id = id,
                Contract = contractDto 
            });

            return Ok(await Mediator.Send(new GetContractDetailsQuery { Id = id }));
        }

        // DELETE: api/contracts/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteContract([FromRoute] int id)
        {
            return Ok(await Mediator.Send(new DeleteContractCommand { Id = id }));
        }

        // GET: api/contracts/types
        [HttpGet("types")]
        public async Task<ActionResult<ContractTypeListViewModel>> GetContractTypes()
        {
            return Ok(await Mediator.Send(new GetContractTypesQuery()));
        }
    }
}

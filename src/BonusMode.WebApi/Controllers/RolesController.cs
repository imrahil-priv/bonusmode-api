using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using BonusMode.Application.Roles.Models;
using BonusMode.Application.Roles.Queries.GetRolesList;

namespace BonusMode.WebApi.Controllers
{
    public class RolesController : BaseController
    {
        // GET: api/roles
        [HttpGet]
        public async Task<ActionResult<RolesListViewModel>> GetAll()
        {
            return Ok(await Mediator.Send(new GetRolesListQuery()));
        }
    }
}

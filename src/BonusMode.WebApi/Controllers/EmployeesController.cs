using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BonusMode.Persistence;
using BonusMode.Application.Employees.Models;
using BonusMode.Domain.Entities;
using BonusMode.Application.Employees.Commands.CreateEmployee;
using BonusMode.Application.Employees.Commands.DeleteEmployee;
using BonusMode.Application.Employees.Commands.UpdateEmployee;
using BonusMode.Application.Employees.Queries.GetEmployeesList;
using BonusMode.Application.Employees.Queries.GetEmployeeDetails;

namespace BonusMode.WebApi.Controllers
{
    public class EmployeesController : BaseController
    {
        // GET: api/employees
        [HttpGet]
        public async Task<ActionResult<EmployeeListViewModel>> GetAllEmployees()
        {
            return Ok(await Mediator.Send(new GetEmployeesListQuery()));
        }

        // GET: api/employees/5
        [HttpGet("{id}")]
        public async Task<ActionResult<EmployeeDto>> GetEmployeeDetails([FromRoute] int id)
        {
            return Ok(await Mediator.Send(new GetEmployeeDetailsQuery { Id = id }));
        }

        // POST: api/employees
        [HttpPost]
        public async Task<IActionResult> CreateEmployee([FromBody] EmployeeDto employeeDto)
        {
            await Mediator.Send(new CreateEmployeeCommand { Employee = employeeDto });

            return NoContent();
        }

        // PUT: api/employees/5
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateEmployee([FromRoute] int id, [FromBody] EmployeeDto employeeDto)
        {
            await Mediator.Send(new UpdateEmployeeCommand { 
                Id = id,
                Employee = employeeDto 
            });

            return NoContent();
        }

        // DELETE: api/employees/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee([FromRoute] int id)
        {
            return Ok(await Mediator.Send(new DeleteEmployeeCommand { Id = id }));
        }
    }
}

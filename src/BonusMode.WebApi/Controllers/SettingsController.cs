using System.Threading.Tasks;
using BonusMode.Application.Settings.Commands.UpdateSettings;
using BonusMode.Application.Settings.Models;
using BonusMode.Application.Settings.Queries.GetSettings;
using Microsoft.AspNetCore.Mvc;

namespace BonusMode.WebApi.Controllers
{
    public class SettingsController : BaseController
    {
        // GET: api/settings
        [HttpGet]
        public async Task<ActionResult<SettingDto>> GetSettings()
        {
            return Ok(await Mediator.Send(new GetSettingsQuery()));
        }

        // PUT: api/settings
        [HttpPut]
        public async Task<IActionResult> UpdateSettings([FromBody] SettingDto settingDto)
        {
            await Mediator.Send(new UpdateSettingsCommand { Setting = settingDto });

            return Ok(await Mediator.Send(new GetSettingsQuery ()));
        }
    }
}

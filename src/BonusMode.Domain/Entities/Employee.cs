﻿using System;
using System.Collections.Generic;

namespace BonusMode.Domain.Entities
{
    public partial class Employee
    {
        public Employee()
        {
            ContractEmployees = new HashSet<ContractEmployee>();
        }

        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int RoleId { get; set; }

        public Role Role { get; set; }
        public ICollection<ContractEmployee> ContractEmployees { get; private set; }
    }
}

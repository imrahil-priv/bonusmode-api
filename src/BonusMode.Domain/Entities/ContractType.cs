﻿using System;
using System.Collections.Generic;

namespace BonusMode.Domain.Entities
{
    public partial class ContractType
    {
        public ContractType()
        {
            Contracts = new HashSet<Contract>();
        }

        public int ContractTypeId { get; set; }
        public string Name { get; set; }

        public ICollection<Contract> Contracts { get; private set; }
    }
}

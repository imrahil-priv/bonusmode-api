namespace BonusMode.Domain.Entities
{
    public class Setting
    {
        public int SettingId { get; set; }

        public float ValuationBonus { get; set; }
        public float MeasurementBonus { get; set; }
        public float FinalizationBonus { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace BonusMode.Domain.Entities
{
    public partial class Role
    {
        public Role()
        {
            Employees = new HashSet<Employee>();
        }

        public int RoleId { get; set; }
        public string RoleName { get; set; }

        public ICollection<Employee> Employees { get; private set; }
    }
}

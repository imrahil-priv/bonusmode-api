﻿using System;
using System.Collections.Generic;

namespace BonusMode.Domain.Entities
{
    public partial class Contract
    {
        public Contract()
        {
            ContractEmployees = new HashSet<ContractEmployee>();
        }

        public int ContractId { get; set; }
        public string Number { get; set; }
        public string Description { get; set; }
        public float Amount { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool Finished { get; set; }
        public bool Accounted { get; set; }
        public int ContractTypeId { get; set; }

        public ContractType ContractType { get; set; }
        public ICollection<ContractEmployee> ContractEmployees { get; private set; }
    
        public Guid Guid { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace BonusMode.Domain.Entities
{
    public partial class ContractEmployee
    {
        public int ContractEmployeeId { get; set; }
        public int ContractId { get; set; }
        public int EmployeeId { get; set; }
        public int ScopeValuation { get; set; }
        public int ScopeMeasurement { get; set; }

        public Contract Contract { get; set; }
        public Employee Employee { get; set; }
    }
}

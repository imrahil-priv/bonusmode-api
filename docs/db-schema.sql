-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: db
-- Czas generowania: 27 Lis 2018, 21:25
-- Wersja serwera: 8.0.13
-- Wersja PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `bonusmode`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ContractEmployee`
--

CREATE TABLE `ContractEmployee` (
  `ContractEmployeeId` int(11) NOT NULL,
  `ContractId` int(11) NOT NULL,
  `EmployeeId` int(11) NOT NULL,
  `ScopeValuation` int(11) NOT NULL DEFAULT '0',
  `ScopeMeasurement` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ContractEmployee`
--

INSERT INTO `ContractEmployee` (`ContractEmployeeId`, `ContractId`, `EmployeeId`, `ScopeValuation`, `ScopeMeasurement`) VALUES
(1, 1, 1, 100, 100),
(2, 1, 2, 100, 100),
(3, 2, 3, 50, 50),
(4, 3, 3, 50, 50),
(5, 3, 2, 50, 50),
(6, 5, 10, 50, 50),
(10, 10, 2, 50, 50),
(11, 11, 2, 50, 50),
(12, 12, 2, 150, 50),
(16, 12, 1, 50, 150),
(17, 12, 3, 150, 50),
(18, 15, 1, 100, 100),
(19, 16, 2, 123, 123),
(20, 17, 1, 50, 0),
(21, 17, 2, 50, 100),
(22, 18, 10, 50, 50),
(23, 18, 2, 50, 50),
(24, 19, 2, 100, 100);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Contracts`
--

CREATE TABLE `Contracts` (
  `ContractId` int(11) NOT NULL,
  `Number` varchar(45) NOT NULL,
  `Description` text,
  `Amount` float NOT NULL,
  `CreatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UpdatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Finished` bit(1) NOT NULL DEFAULT b'0',
  `Accounted` bit(1) NOT NULL DEFAULT b'0',
  `ContractTypeId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Contracts`
--

INSERT INTO `Contracts` (`ContractId`, `Number`, `Description`, `Amount`, `Finished`, `Accounted`, `ContractTypeId`) VALUES
(1, '1/2018', 'Lorem ipsum dolor sit amet.', 1200, b'0', b'0', 1),
(2, '4/2018', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed in suscipit dui.', 100, b'1', b'1', 1),
(3, '10/2018', 'string2', 100, b'0', b'0', 1),
(4, '24/2018', 'string3', 100, b'0', b'0', 1),
(5, '27/2018', 'string3', 100, b'0', b'0', 1),
(10, '28/2018', 'string', 1300, b'0', b'0', 2),
(11, '30/2018', 'string', 1300, b'0', b'0', 2),
(12, '35/2018', 'Testowy testowy', 1300, b'0', b'0', 1),
(15, '123', 'test', 1234, b'0', b'0', 1),
(16, 'grrr', '123', 123, b'0', b'0', 1),
(17, '234', 'test', 12344, b'0', b'0', 1),
(18, '123', 'test', 123434, b'0', b'0', 1),
(19, '234', 'test 2', 123.12, b'0', b'0', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ContractTypes`
--

CREATE TABLE `ContractTypes` (
  `ContractTypeId` int(11) NOT NULL,
  `Name` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `ContractTypes`
--

INSERT INTO `ContractTypes` (`ContractTypeId`, `Name`) VALUES
(1, 'Standard'),
(2, 'Email'),
(3, 'Facebook');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Employees`
--

CREATE TABLE `Employees` (
  `EmployeeId` int(11) NOT NULL,
  `FirstName` varchar(65) NOT NULL,
  `LastName` varchar(65) NOT NULL,
  `RoleId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Employees`
--

INSERT INTO `Employees` (`EmployeeId`, `FirstName`, `LastName`, `RoleId`) VALUES
(1, 'Jan', 'Kowalski', 1),
(2, 'Tomasz', 'Wiśniewski', 2),
(3, 'Admin', 'Admin', 3),
(10, 'string 2', 'string', 1),
(12, 'Księgowa', 'Magdalena', 4),
(13, 'Krzysztof', 'Pomiarowiec', 2),
(14, 'Jarek', 'Szczepański', 1);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `Roles`
--

CREATE TABLE `Roles` (
  `RoleId` int(11) NOT NULL,
  `RoleName` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `Roles`
--

INSERT INTO `Roles` (`RoleId`, `RoleName`) VALUES
(1, 'Sprzedawca'),
(2, 'Pomiarowiec'),
(3, 'Administrator'),
(4, 'Księgowa');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `__EFMigrationsHistory`
--

CREATE TABLE `__EFMigrationsHistory` (
  `MigrationId` varchar(95) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Zrzut danych tabeli `__EFMigrationsHistory`
--

INSERT INTO `__EFMigrationsHistory` (`MigrationId`, `ProductVersion`) VALUES
('20181111071305_Init', '2.1.4-rtm-31024'),
('20181111213331_BetterConstraintAndNewColumn', '2.1.4-rtm-31024');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `ContractEmployee`
--
ALTER TABLE `ContractEmployee`
  ADD PRIMARY KEY (`ContractEmployeeId`),
  ADD KEY `fk_ContractEmployee_1_idx` (`ContractId`),
  ADD KEY `fk_ContractEmployee_2_idx` (`EmployeeId`);

--
-- Indeksy dla tabeli `Contracts`
--
ALTER TABLE `Contracts`
  ADD PRIMARY KEY (`ContractId`),
  ADD KEY `fk_Contract_1_idx` (`ContractTypeId`);

--
-- Indeksy dla tabeli `ContractTypes`
--
ALTER TABLE `ContractTypes`
  ADD PRIMARY KEY (`ContractTypeId`);

--
-- Indeksy dla tabeli `Employees`
--
ALTER TABLE `Employees`
  ADD PRIMARY KEY (`EmployeeId`),
  ADD KEY `fk_Employees_1_idx` (`RoleId`);

--
-- Indeksy dla tabeli `Roles`
--
ALTER TABLE `Roles`
  ADD PRIMARY KEY (`RoleId`);

--
-- Indeksy dla tabeli `__EFMigrationsHistory`
--
ALTER TABLE `__EFMigrationsHistory`
  ADD PRIMARY KEY (`MigrationId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `ContractEmployee`
--
ALTER TABLE `ContractEmployee`
  MODIFY `ContractEmployeeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT dla tabeli `Contracts`
--
ALTER TABLE `Contracts`
  MODIFY `ContractId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT dla tabeli `ContractTypes`
--
ALTER TABLE `ContractTypes`
  MODIFY `ContractTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `Employees`
--
ALTER TABLE `Employees`
  MODIFY `EmployeeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT dla tabeli `Roles`
--
ALTER TABLE `Roles`
  MODIFY `RoleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `ContractEmployee`
--
ALTER TABLE `ContractEmployee`
  ADD CONSTRAINT `fk_ContractEmployee_ContractId` FOREIGN KEY (`ContractId`) REFERENCES `Contracts` (`contractid`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_ContractEmployee_EmployeeId` FOREIGN KEY (`EmployeeId`) REFERENCES `Employees` (`employeeid`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `Contracts`
--
ALTER TABLE `Contracts`
  ADD CONSTRAINT `fk_Contract_1` FOREIGN KEY (`ContractTypeId`) REFERENCES `ContractTypes` (`contracttypeid`) ON DELETE RESTRICT ON UPDATE CASCADE;

--
-- Ograniczenia dla tabeli `Employees`
--
ALTER TABLE `Employees`
  ADD CONSTRAINT `fk_Employees_1` FOREIGN KEY (`RoleId`) REFERENCES `Roles` (`roleid`) ON DELETE RESTRICT ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
